# ATLAN Backend Task
### Problem Statement

Design a sample schematic for how you would store forms (with questions) and responses (with answers) in the data store. Forms, Questions, Responses and Answers each will have relevant metadata. Design and implement a solution for the Google Sheets use case and choose any one of the others to keep in mind how we want to solve this problem in a plug-n-play fashion. Make fair assumptions wherever necessary.


## 🎉 Day - 1 
### Understanding

#### 1. Initial Thoughts

To build a Form submission platform, that is fast, and can tolerate huge number of request. Is fault tolerant and is able to handle multiple edgecases

**Edge Cases**

1. To make sure that the data entered are in proper format (checks)
2. Data is cached and is safe from power loss, internet loss, service outages
3. should **scale to cases like millions of responses** across hundreds of forms for an organization.

### Features to be Implemented

1. Optimize Latency
2. Make it scalable( can be used by millions of users )
    1. API gateways and Loadbalancer
    2. Databse scaling
3. Unified Interface
4. Failsafe(recover from circumstances like power/internet/service outages,)
    1. Retry mechanisms
5. SMS notifications
    1. Message Queues
6. Benchmarking
7. Testing
8. Deploying

### Proposed Solution(Possible)?

#### Solution 1:

 ![image]("./assets/day1.png")

1. Clients
2. API Gateway 
    1. Why needed
    2. Pros and Cons
    3. Other options
3. Loadbalancer
    1. Why needed
    2. Pros and cons 
    3. other options
4. Dockerising the app 
    1. Why needed
    2. Pros and cons
5. Databases
    1. Why NoSQL
    2. Pros and Cons
    3. Other Options
6. Messaging Queues
7. Bench

## Final Solution?