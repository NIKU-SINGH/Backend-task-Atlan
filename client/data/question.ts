export interface InterviewQuestion {
  question_id: number;
  question_text: string;
}

const question: InterviewQuestion[]  = [
    {
      "question_id": 1,
      "question_text": "Tell me about yourself."
    },
    {
      "question_id": 2,
      "question_text": "What is your major and specialization?"
    },
    {
      "question_id": 3,
      "question_text": "What is your current cumulative GPA (CGPA)?"
    }
  ]

  export default question
  