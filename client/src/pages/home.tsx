import { Button } from "@/components/ui/button";
import Form, { getForm } from "./components/form2";
import { google } from "googleapis";
import bg from "./assets/bg.svg";
import axios from "axios";
import { useQuery } from "react-query";
import { Routes, Route, Link } from "react-router-dom";

interface FormData {
  _id: string;
  description: string;
  title: string;
  questions: Question[];
  createdAt: string;
  updatedAt: string;
  __v: number;
}

interface Question {
  question: string;
  _id: string;
}

const getAllForm = async () => {
  try {
    const form = await axios.get(`${URL}/api/v1/form/all/data`);
    console.log(form.data);
    return form.data;
  } catch (err) {
    return err;
  }
};
const URL = "http://localhost:8080";

export default function Home() {
  const { data, isError, isLoading } = useQuery("getAllForm", getAllForm);
  console.log("data",data)
  if (isLoading || isError) {
    return <h1>Error while loading</h1>;
  }

  return (
    // 
    <div className="bg-gray-100 h-[100vh] w-[100vw]">
      <h1 className="text-center p-10 text-3xl font-medium">
        A list of Forms that you can fill
      </h1>
      <div className="flex flex-row flex-wrap p-10">
        {data.map((form: FormData) => (
          // Single card
          <div
            key={form._id}
            className="max-w-sm m-4 rounded-2xl hover:cursor-pointer overflow-hidden shadow-lg"
          >
            <img
              className="object-cover"
              src="https://images.unsplash.com/photo-1519681393784-d120267933ba?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80"
              alt="header"
            />
            <div className="px-6 py-4">
              <div className="font-bold text-xl mb-2">{form.title}</div>
              <p className="text-gray-700 text-base">{form.description}</p>
            </div>
            <div className="px-6 pt-4 pb-2">
              <Link to={`form/${form._id}`}>
                <button className="bg-blue-800 p-2 rounded my-4 text-white">
                  Fill the form
                </button>
              </Link>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
