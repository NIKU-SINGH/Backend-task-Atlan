import React, { useEffect, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { z } from "zod";
import question from "../../data/question";
import { InterviewQuestion } from "../../data/question";
import axios from "axios";
import { useQuery } from "react-query";
import { zodResolver } from "@hookform/resolvers/zod";
import bg from "../assets/bg.svg";

// React router dom
import { useParams } from "react-router-dom";

// Schema for Forms
const FormSchema = z.object({
  username: z.string().min(2, {
    message: "Username must be at least 2 characters.",
  }),
  email: z
    .string({
      required_error: "Email is Required",
      invalid_type_error: "Email must contain @",
    })
    .email({
      message: "Invalid email address,Email must contain @",
    }),
  form_id: z.string(),
  answers: z.array(
    z.object({
      question_id: z.string(),
      question: z.string(),
      answer: z.string(),
    })
  ),
});

const URL = "http://localhost:8080";

const AnswerForm = () => {
  const { form_id } = useParams();
  const getForm = async () => {
    try {
      const form = await axios.get(`${URL}/api/v1/form/${form_id}`);
      console.log(form.data);
      return form.data;
    } catch (err) {
      return err;
    }
  };
  const { data, isError, isLoading } = useQuery("getForm", getForm);
  // Submit handler
  const { handleSubmit, control, formState, register } = useForm({
    resolver: zodResolver(FormSchema),
  });

  const { errors } = formState;
  console.log("This is the error", errors);
  if (isLoading || isError) {
    return <h1>Error while loading</h1>;
  }

  const onSubmit = async (data) => {
    console.log("Data sent as a response", data);
    localStorage.setItem(data.email, JSON.stringify(data));
    try {
      const res = await axios.post(`${URL}/api/v1/form/${form_id}`, data);
      const ans = res?.data;
      console.log("data received from server", ans);
    } catch (err) {
      console.error("Error", err?.message);
    }
  };

  const savedResponse = localStorage.getItem(data.email);
  useEffect(() => {}, []);

  return (
    <div
      className=" flex flex-col items-center justify-center p-10"
      style={{
        backgroundImage: `url(${bg})`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
    >
      <form
        className="bg-slate-100 rounded-xl md:w-1/3 lg:1/3 p-8"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div>
          <img
            className="h-64 rounded-xl w-full object-cover"
            src="https://images.unsplash.com/reserve/bOvf94dPRxWu0u3QsPjF_tree.jpg?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2076&q=80"
          />
        </div>
        <div className="my-4">
          <p className="text-3xl text-center font-medium">{data.title}</p>
        </div>
        <div className="flex">
          <Controller
            name={`form_id`}
            control={control}
            defaultValue={data._id}
            render={({ field }) => <input {...field} type="hidden" />}
          />
        </div>

        <div className="my-4">
          <p>{data.description}</p>
        </div>
        <div className="flex flex-col space-y-4X my-4">
          <label className="text-lg font-medium">Username:</label>
          <input
            className="w-full p-2 bg-gray-200 rounded-lg"
            {...register("username")}
          />
          {errors.username?.message && (
            <span className="text-red-700 font-bold">
              {errors.username.message}
            </span>
          )}
        </div>
        <div className="flex flex-col space-y-4 my-2">
          <label className="text-lg font-medium">Email:</label>
          <input
            className="p-2 bg-gray-200 rounded-lg"
            {...register("email")}
          />
          {errors.email?.message && (
            <span className="text-red-700 font-bold">
              {errors.email.message}
            </span>
          )}
        </div>
        {data?.questions?.map((q, index) => (
          <div className="flex flex-col space-y-4 my-4" key={q._id}>
            <label className="text-lg font-medium">{q.question}</label>
            {/* For question Id */}
            <Controller
              name={`answers[${index}].question_id`}
              control={control}
              defaultValue={q._id}
              render={({ field }) => <input {...field} type="hidden" />}
            />

            {/* For question*/}
            <Controller
              name={`answers[${index}].question`}
              control={control}
              defaultValue={q.question}
              render={({ field }) => <input {...field} type="hidden" />}
            />

            {/* For answer*/}
            <Controller
              name={`answers[${index}].answer`}
              control={control}
              defaultValue=""
              render={({ field }) => (
                <input
                  className=" bg-gray-200 rounded-lg h-10 px-2"
                  {...field}
                  type="text"
                />
              )}
            />
          </div>
        ))}
        <button
          className="bg-blue-900 p-4 rounded-xl text-white px-10"
          type="submit"
        >
          Submit
        </button>
      </form>
    </div>
  );
};

export default AnswerForm;
