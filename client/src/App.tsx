import React from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Home from "./pages/home";
import Form from './components/form'
import CreateForm from './components/createForm'
export default function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="form/:form_id" element={<Form />} />
          <Route path="admin/create" element={<CreateForm />} />
          {/* <Route path="/error-page" element={<ErrorPage />} /> */}
        </Routes>
      </BrowserRouter>
    </div>
  );
}
