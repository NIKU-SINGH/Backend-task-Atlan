# FormsApp

This is a streamlined yet robust forms application that efficiently manages multiple concurrent requests, utilizing NGINX as a load balancer, Redis as a caching solution, and Docker for container orchestration.

## Contents

1. [Introduction](#-introduction)
2. [About the Project](#-about-the-project)
3. [Problem Statement](#-problem-statement)
4. [Features Implemented](#-features-implemented)
5. [Demo Video](#-demo-video)
6. [Solution Proposed](#-solution-proposed)
7. [Proposed Architecture](#-proposed-architecture)
8. [Technologies Used](#-technologies-used)
9. [Why this Architecture Stands Out Best](#-why-this-architecture-stands-out-best)
10. [Benchmarking](#-benchmarking)
11. [Other Ways to Solve](#-other-ways-to-solve)
12. [Possible Bottlenecks](#-possible-bottlenecks)
13. [How it Can be Improved Further](#-how-it-can-be-improved-further)
14. [My Learnings](#-my-learnings)
15. [Commit Histories](#-commit-histories)
16. [References Used](#-references-used)
17. [Note of Thanks](#-note-of-thanks)

## 🌐 Introduction

- Name: Niku Singh
- Email: nikusingh319@gmail.com, soapmactavishmw4@gmail.com
- Github Username: NIKU-SINGH
- LinkedIn: https://www.linkedin.com/in/niku-singh/
- Twitter: https://twitter.com/Niku_Singh_
- University: Dr B R Ambedkar National Institute of Technology, Jalandhar


## 🤔 Problem Statement

Design a sample schematic for how you would store forms (with questions) and responses (with answers) in the data store. Forms, Questions, Responses and Answers each will have relevant metadata. Design and implement a solution for the Google Sheets use case and choose any one of the others to keep in mind how we want to solve this problem in a plug-n-play fashion. Make fair assumptions wherever necessary.

Eventual consistency is what the clients expect as an outcome of this feature, making sure no responses get missed in the journey. Do keep in mind that this solution must be failsafe, should eventually recover from circumstances like power/internet/service outages, and should scale to cases like millions of responses across hundreds of forms for an organization.

## ⚙️ Features Implemented

- [x] Client Side for the Forms
- [x] Storing Data in Database
- [x] Storing Data in the Excell sheet
- [ ] Sending Messages after the form Completion

## 📹 Demo Video
https://www.loom.com/share/865c0c1d1dfd435b8693a52e87f76294?sid=6b881548-09a1-40a1-97ae-0cf142edc4b9

# 💡 Solution Proposed

## 🏛️ Proposed Architecture

![architecture](./assets/image.png)

- After spending a considerable amount of time in the researh I found the above arhitecture to be suitable for handling more than 1 Million + users.

## 💻 Technologies Used

### Frontend

| Technology Used                                  | Reason             |
| ------------------------------------------------ | ------------------ |
| [ReactJS](https://reactjs.org/)                  | UI Development     |
| [Vite](https://vitejs.dev/)                      | Fast Development   |
| [React Hook Form](https://react-hook-form.com/)  | Form Handling      |
| [Zod](https://zod.dev/)                          | Type Checking      |
| [React Query](https://react-query.tanstack.com/) | Data Fetching      |
| [ShadCN UI library](https://ui.shadcn.com/)      | UI Components      |
| [Tailwind CSS](https://tailwindcss.com/)         | Styling Efficiency |
| [Axios](https://axios-http.com/)                 | HTTP Requests      |

### Backend

| Technology Used                        | Reason                                                   |
| -------------------------------------- | -------------------------------------------------------- |
| [NodeJS](https://nodejs.org/)          | Server-Side JavaScript and Backend Development           |
| [Express](https://expressjs.com/)      | Minimalist Web Application Framework for Node.js         |
| [MongoDB](https://www.mongodb.com/)    | NoSQL Database for Flexible Data Storage                 |
| [Mongoose](https://mongoosejs.com/)    | Elegant MongoDB Object Modeling for Node.js              |
| [Pino](https://github.com/pinojs/pino) | Fast and Low Overhead Node.js Logger                     |
| [Redis](https://redis.io/)             | In-Memory Data Store for Caching and Real-Time Analytics |
| [Docker](https://www.docker.com/)      | Container Orchestration                                  |
| [NGINX](https://www.nginx.com/)        | Load Balancer and Web Server                             |

<!-- ### Backend
[![My Skills](https://skillicons.dev/icons?i=nodejs,express,docker,redis,nginx,)](https://skillicons.dev) -->

## 🏆 Why this Architecture Stands out best

<!-- The architecture makes use of ReactJS for the Client side and NodeJS for the Server side along with NGINX for ladebalancing and Docker compose to run multiple instances of the server. Redis as the cache and MongoDB as a Database. -->

### 1. Client Side

Have used ReactJS for the client side

1. **Why needed**
   - RectJS is fast and has component based architecture
   - Reacts Virtula DOM optimizes performance. It allows React to update only the parts of the actual DOM that have changed, rather than re-rendering the entire DOM tree.

### 2. NGINX as a Loadbalancer

1. **Why NGINX was needed**
   - NGINX is used as a layer 7 load balancer to route the traffic to different servers using Round Robin Algorithm.
   - NGINX is capable of handling 512 concurrent request in a single process.
2. **Pros**

   - NGINX is a versatile web sever and can be used as a load balancer, web server & Reverse proxy.
   - NGINX offers L4 and L7 load balancers.
   - Its Highly Performant and low resource usage

3. **Cons**

   - NGINX may still consume a significant amount of memory when handling a large number of connections
   - NGINX has a complex configuration

4. **Other Options**
   - HAproxy
   - Envoy
   - Caddy

### 3. Running Multiple Docker containers

1. **Why was Docker Needed**
   - Docker is used to create an image of the app and run it on an isolated container
   - We can easily spin up and manage all the required services, such as the server, database, and other components, by defining the configuration in a single compose file.

### 4. Database

1. **Why MongoDB is chosen**

   - MongoDB is used to store Forms, Questions and Responses by the user.
   - As I needed flexibility with the schema so I chose MongoDB.
   - NoSQL over SQL because forms responses may lack proper structure.
   - NoSQL provides scalability

2. **Pros**

   - NoSQL database allows to store data in a more dynamic, schema-less manner, which is valuable for applications with changing data requirements.
   - MongoDB is Horizontally Scalable.
   - High Performance data retrieval.
   - MongoDB stores data in BSON (Binary JSON) format, which is similar to JSON. This makes it easy to work with data.
   - MongoDB supports automatic data sharding, which allows for horizontal partitioning of data across multiple servers.
   - Easier to work with

3. **Cons**
   - Lack of ACID Transactions, which leads to data inconsistency.
   - No Built-in Joins: MongoDB does not support traditional SQL-style joins between collections.

### 6. Redis

1. **Why Caching is needed**
   - I decided to cache the response of forms so that in case of failure and power outage the data can be retrived back from the Cache
2. **Why Redis is chosen**
   - Redis is an in-memory data store
   - Making it extremely fast read and write operations
   - Redis provides a rich set of data structures, such as strings, lists, sets, sorted sets, hashes, and more.
   - Low latency
3. **Pros of Caching**

   - Save network calls (reduces more calls to the DB)
   - Avoid recomputation (store the frequent computed task on the server)
   - Reduce DB load
   - Helps speed up responses

4. **Cons of Caching**
   - Will be making an extra call, if the cache doesnot have a data it would be waste of time
   - Thrashing: Inserting and Removing the datas in the cache frequently
   - Data consistency issue

## 📊 Benchmarking

For testing a Node.js application to assess its performance and scalability some of the tools that I can use are

1. Artillery
2. K6

## ⚠️ Possible Bottlenecks

While this architecture is the know to be the best that I can think of, but of course there are various bottlenecks in this architecture some of the areas where I think it can fail is.

1. At current the Redis is caching behing the servers which is not that good and reduces the performance, possible way to fix is we can cache it on the server or the at the loadbalancer level.
2. The Database and Caches are Single point of failure and we would need to make multiple copies so that the Sytem becomes fault tolerant.

## 💡 Other ways to Solve

Some other Architecture that I though can solve the problem are 
1. 

## 🔄 How it can be Improved Further

1. Master Slave model for the Server : Making use of multiple databse and Defining a Master Slave relationship. 
2. Master being the primary databse for writng the enteries and Slaves being the database that sync with the master and mainly used for reading purpose

3. Inmemory caching : making cache on the server
	1. **Pros**
		1. Caching is closer to the server & hence faster
	2. **Cons**
		1. What if the server fails then the datas are lost
		2. Server1 and Server2 doesnt have consistency of data


## 📚 My Learnings

While preparing for the Assignment I had the opportunity to learn a lot of new stuffs and Technologies. Some of my learning were

1. I got to learn about how to build resilient and fault tolerant scalable softwares
2. Learned about loadbalacning and caching.
3. Learned about docker and how to dockerizing the app.
4. Learned how to inegrate sheets API and use it as a simple database.

## 📝 Commit Histories

| Commit                                                                                                                                       | Description                                       |
| -------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------- |
| [cad2f00ebebc036c0b97c5a13bf226643a8f0916](https://github.com/NIKU-SINGH/Backend-task-Atlan/commit/cad2f00ebebc036c0b97c5a13bf226643a8f0916) | 🚀 ✨ ⚡️ Initial setup of client and server      |
| [c32a4975c01c26e145e7b1531d32ceb7d59e5199](https://github.com/NIKU-SINGH/Backend-task-Atlan/commit/c32a4975c01c26e145e7b1531d32ceb7d59e5199) | 🚀 ⚡️ ✨ Added Models and Connected to DB        |
| [6b15e0d1b12d02cfc5e76f93a81a232efdb59951](https://github.com/NIKU-SINGH/Backend-task-Atlan/commit/6b15e0d1b12d02cfc5e76f93a81a232efdb59951) | 🎉 🐛dockerising the app                          |
| [06f26318f6a2f3e6930a3ce1ee965fef5d11087f](https://github.com/NIKU-SINGH/Backend-task-Atlan/commit/06f26318f6a2f3e6930a3ce1ee965fef5d11087f) | Created home page and forms page, added apis      |
| [4d5820dcc79827f0d3faae2d13534df9cbf1d252](https://github.com/NIKU-SINGH/Backend-task-Atlan/commit/4d5820dcc79827f0d3faae2d13534df9cbf1d252) | 🏗️ ✨ 🐛 added complete Sheets API integration    |
| [8e714c80c3dd57abb4e1b92fa1aa7f5b0d465546](https://github.com/NIKU-SINGH/Backend-task-Atlan/commit/8e714c80c3dd57abb4e1b92fa1aa7f5b0d465546) | 🦺 🎨 📝 updated documentation and server         |
| [02109d9f1bff3749e673739bdc91cf85bc5592cc](https://github.com/NIKU-SINGH/Backend-task-Atlan/commit/02109d9f1bff3749e673739bdc91cf85bc5592cc) | ♿️ 🔥 ✨ added zod resolver and client side test |

## 📖 References Used

- https://hackernoon.com/how-to-use-google-sheets-api-with-nodejs-cz3v316f
- https://towardsdatascience.com/sample-load-balancing-solution-with-docker-and-nginx-cf1ffc60e644
- [Alex Xu Volume 1: System Design Interview - An insider's guide](https://www.amazon.in/System-Design-Interview-insiders-Second/dp/B08CMF2CQF)
