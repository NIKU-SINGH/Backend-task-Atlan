import mongoose from "mongoose";
import logger from '../utils/logger'

const connection = async () => {
    const URI = process.env.MONGO_URL;
    await mongoose.connect(URI);
    logger.info(`Connection established successfully`);

};

mongoose.connection.on("disconnect", () => {
    logger.info(`MongoDB Disconnected`);
});

mongoose.connection.on("connected", () => {
    logger.info(`MongoDB connected`);
});

export default connection;
