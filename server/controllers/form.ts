import Form from "../models/form";
import { Request, Response } from "express";
import userData from "../models/responses";
import { ResponseModel } from "../models/responses";
import logger from "../utils/logger";
import { addValuesToSpreadSheet,createNewSpreahSheet } from "../sheets/sheets";

interface Form {
  description: string;
  title: string;
  questions: Question[];
  _id: string;
  createdAt: string;
  updatedAt: string;
  __v: number;
}

interface Question {
  question: string;
  _id: string;
}

const createForm = async (req: Request, res: Response) => {
  try {
    const data = req.body;
    const form = new Form(data);
    const formData = await form.save();
    // logger.info(formData)
    createNewSpreahSheet(data);
    res.status(200).json(formData);
  } catch (err) {
    logger.error(err);
    res.status(500).json(err);
  }
};

const saveResponse = async (req: Request, res: Response) => {
  try {
    const data = req.body;
    // send values to spreadsheet
    addValuesToSpreadSheet(data);
    
    const saved = new userData(data);
    const returningData = await saved.save();
    // console.log(returningData);
    res.status(200).json(returningData);
  } catch (err) {
    logger.error(err);
    res.status(500).json(err);
  }
};

const getForm = async (req: Request, res: Response) => {
  try {
    const { form_id } = req.params;
    const form = await Form.findById(form_id);
    res.status(200).json(form);
  } catch (err) {
    logger.error(err);
    res.status(500).json(err);
  }
};

const getAllForms = async (req: Request, res: Response) => {
  try {
    const forms = await Form.find();
    res.status(200).json(forms); // Send the forms as JSON response
  } catch (err) {
    logger.error(err);
    res.status(500).json({ error: "An error occurred while fetching forms." });
  }
};

export { createForm, saveResponse, getForm, getAllForms };
