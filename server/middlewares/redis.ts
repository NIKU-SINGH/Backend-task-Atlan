import { createClient } from "redis";

export const redisclient = createClient();

redisclient.on('error', err => console.log('Redis Client Error', err));

export default function cache(req, res, next) {
  const data = req.body.data
  const key = data;
  // console.log("Cache",key)

  redisclient
    .get(key)
    .then((reply) => {
      if (reply) {
        res.send(JSON.parse(reply));
      } else {
        res.sendResponse = res.send;
        res.send = (body) => {
          //expire in 1 min
          redisclient.set(key, JSON.stringify(body), { EX: 60 });
          res.sendResponse(body);
        };
        next();
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send(err);
    });
}
