import { google } from "googleapis";
import logger from "../utils/logger";
import fs from "fs";
import path from "path";
const sheets = google.sheets("v4");

// Define the interface for Google Sheets API parameters
interface SheetsAppendParams {
  auth: any;
  spreadsheetId: string;
  range: string;
  valueInputOption: string;
  insertDataOption: string;
  resource: {
    values: any[][];
  };
}

// Define the OAuth2 scopes required
const SCOPES = ["https://www.googleapis.com/auth/spreadsheets"];

// Your spreadsheet ID
const spreadsheetId = "1o-UHOedHX-mUd3bZvbnvL7Bya7gU15aklLekADMSf8w";

// Function to get OAuth2 authentication token
async function getAuthToken() {
  // Create Google Auth client
  const auth = new google.auth.GoogleAuth({
    scopes: SCOPES,
    keyFilename: path.join(__dirname, "..", process.env.GOOGLE_APPLICATION_CREDENTIALS),
  });

  // Get the authentication token
  const authToken = await auth.getClient();
  return authToken;
}

// Function to create a new sheet with questions
async function createNewSpreahSheet(data) {
  const auth = await getAuthToken();
  const { questions } = data;

  // Create a row with question labels and add user-related columns
  let questionsRow = [];
  questionsRow = questions.map((item) => item.question);
  questionsRow.unshift("username", "email", "form_id");

  const params: SheetsAppendParams = {
    auth,
    spreadsheetId: spreadsheetId,
    range: "Sheet1",
    valueInputOption: "RAW",
    insertDataOption: "INSERT_ROWS",
    resource: {
      values: [questionsRow],
    },
  };

  try {
    const response = await sheets.spreadsheets.values.append(params);
    logger.info(`Appended: ${response.data.updates.updatedCells} cells`);
  } catch (err) {
    logger.error(err);
  }
}

// Function to add user answers to the spreadsheet
async function addValuesToSpreadSheet(data) {
  const auth = await getAuthToken();
  const { username, email, form_id } = data;

  // Create a row with user answers and add user-related columns
  let answers = [];
  answers = data.answers.map((item) => item.answer);
  answers.unshift(username, email, form_id);

  const params: SheetsAppendParams = {
    auth,
    spreadsheetId: spreadsheetId,
    range: "Sheet1",
    valueInputOption: "RAW",
    insertDataOption: "INSERT_ROWS",
    resource: {
      values: [answers],
    },
  };

  try {
    const response = await sheets.spreadsheets.values.append(params);
    logger.info(`Appended: ${response.data.updates.updatedCells} cells`);
  } catch (err) {
    logger.error(err);
  }
}

export {
  getAuthToken,
  createNewSpreahSheet,
  addValuesToSpreadSheet,
};
