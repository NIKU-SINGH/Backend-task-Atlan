import express from "express";
import {
  createForm,
  saveResponse,
  getForm,
  getAllForms,
} from "../controllers/form";

import {addValuesToSpreadSheet, createNewSpreahSheet} from '../sheets/sheets'
const router = express.Router();

// // API Check
router.use("/healthcheck", (req, res) => {
  res.send(`APIs are running smoothly`);
});

// make it post later on
router.post("/create", createForm);

// save form
router.post("/:id", saveResponse);

// get form
router.get("/:form_id", getForm);

// get all forms
router.get("/all/data", getAllForms);

router.get("/add/sheets",addValuesToSpreadSheet)

export default router;
