import mongoose , {Schema} from "mongoose";


const responseSchema = new Schema({
  question_id: {
    type: String,
    required: true,
  },
  question: {
    type: String,
    required: true,
  },
  answer: {
    type: String,
    required: true,
  },
});

const tempSchema = new Schema({
  formID: {
    type: String,
    unique: true,
    required: true,
  },
  formDescription: {
    type: String,
  },
  username: {
    type: String,
    unique: true,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  responses: [responseSchema],
});

// Create and export the model
export default mongoose.model("Temp", tempSchema);
