import mongoose, { Schema } from "mongoose";

interface answerModel {
    question_id: string;
    question: string;
    answer: string
}

export interface ResponseModel {
    email : string;
    username: string;
    form_id: mongoose.Schema.Types.ObjectId
    answers : answerModel[]
}

const responseSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  form_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Form",
    required: true,
  },
  answers: [
    {
      question_id: {
        type: String,
        required: true,
      },
      question: {
        type: String,
        require: true,
      },
      answer: {
        type: String,
      },
    },
  ],
});

const userData = mongoose.model<ResponseModel>("Response", responseSchema);

export default userData
