import mongoose, {Schema} from 'mongoose';
const questionSchema = new Schema({
  question: {
    type: String,
    required: true
  }
})
// Define the Forms schema
const formSchema = new Schema({
  description: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  questions: [questionSchema]
},{timestamps: true});

const Form = mongoose.model('Form', formSchema);

export default Form
