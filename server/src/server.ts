import express from "express";
import cors from "cors";
import { google } from "googleapis";
import dotenv from "dotenv";
// Redis
import cache from "../middlewares/redis";
import { redisclient } from "../middlewares/redis";
// DB connection
import connection from "../connections/db";
// Routes
import formRoute from "../routes/form";
import logger from "../utils/logger";
// import { createForm, getForm, saveResponse} from '../controllers/form';

const app = express();
app.use(express.json());
app.use(cors());

dotenv.config();
const PORT: number = Number(process.env.PORT) || 8080;
const APPID = process.env.APPID || 1111;
const HOST: string = "0.0.0.0";

// Connectiong to mongoDB
connection();

// use caching

// app.use("/",(req,res) => {
//   res.send(`Server is running on PORT:${PORT} & Host:${HOST}`);
// })
// app.use(cache);




app.use("/api/v1/form", formRoute);


app.listen(PORT, () => {
  logger.info(`Server listening at ${HOST}:${PORT} and saying hello from ${APPID}`);
  redisclient.connect().then(() => {
    logger.info(`Redis is connected`);
  });
});
